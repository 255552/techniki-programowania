﻿// TP_L2_D2.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <windows.h>
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

enum class Beads { red, yellow, blank };

void prB(Beads*** tab, int x, int y, int h, bool floor = false) {
	x--; y--; h--;
	if (!floor) {
		switch (tab[x][y][h]) {
		case Beads::blank:
			SetConsoleTextAttribute(hConsole, 7);
			std::cout << "|";
			break;
		case Beads::yellow:
			SetConsoleTextAttribute(hConsole, 6);
			std::cout << (char)254;
			break;
		case Beads::red:
			SetConsoleTextAttribute(hConsole, 4);
			std::cout << (char)254;
			break;
		}
		SetConsoleTextAttribute(hConsole, 7);
	}
	else {
		switch (tab[x][y][h]) {
		case Beads::blank:
			SetConsoleTextAttribute(hConsole, 39);
			std::cout << "|";
			break;
		case Beads::yellow:
			SetConsoleTextAttribute(hConsole, 38);
			std::cout << (char)254;
			break;
		case Beads::red:
			SetConsoleTextAttribute(hConsole, 36);
			std::cout << (char)254;
			break;
		}
	}
}

void print_board(Beads*** tab, int d) {
	//         1A  
	//          |     B
	//          |     |     C
	//      2A  |     |     |     D
	//       |  |  B  |     |     |
	//       |     |  |  C  |     |
	//   3A  |     |     |  |  D  |
	//    |  |  B  |     |     |  |
	//    |     |  |  C  |     |
	//4A  |     |     |  |  D  |
	// |  |  B  |     |     |  |
	// |     |  |  C  |     |
	// |     |     |  |  D  |
	// |     |     |     |  |
	//       |     |     |
	//             |     |
	//                   |
	//brzydkie ale działa
	system("cls");
	std::cout << "         1A\n";
	std::cout << "          "; prB(tab, 1, 1, 4); std::cout << "     B\n";
	std::cout << "          "; prB(tab, 1, 1, 3); std::cout << "     "; prB(tab, 2, 1, 4); std::cout << "     C\n";
	std::cout << "      2A  "; prB(tab, 1, 1, 2); std::cout << "     "; prB(tab, 2, 1, 3); std::cout << "     "; prB(tab, 3, 1, 4); std::cout << "     D\n";
	std::cout << "       "; prB(tab, 1, 2, 4); std::cout << "  "; prB(tab, 1, 1, 1); std::cout << "  B  "; prB(tab, 2, 1, 2); std::cout << "     "; prB(tab, 3, 1, 3); std::cout << "     "; prB(tab, 4, 1, 4); std::cout << "\n";
	std::cout << "       "; prB(tab, 1, 2, 3); std::cout << "     "; prB(tab, 2, 2, 4); std::cout << "  "; prB(tab, 2, 1, 1); std::cout << "  C  "; prB(tab, 3, 1, 2); std::cout << "     "; prB(tab, 4, 1, 3); std::cout << "\n";
	std::cout << "   3A  "; prB(tab, 1, 2, 2); std::cout << "     "; prB(tab, 2, 2, 3); std::cout << "     "; prB(tab, 3, 2, 4); std::cout << "  "; prB(tab, 3, 1, 1); std::cout << "  D  "; prB(tab, 4, 1, 2); std::cout << "\n";
	std::cout << "    "; prB(tab, 1, 3, 4); std::cout << "  "; prB(tab, 1, 2, 1); std::cout << "  B  "; prB(tab, 2, 2, 2); std::cout << "     "; prB(tab, 3, 2, 3); std::cout << "     "; prB(tab, 4, 2, 4); std::cout << "  "; prB(tab, 4, 1, 1); std::cout << "\n";
	std::cout << "    "; prB(tab, 1, 3, 3); std::cout << "     "; prB(tab, 2, 3, 4); std::cout << "  "; prB(tab, 2, 2, 1); std::cout << "  C  "; prB(tab, 3, 2, 2); std::cout << "     "; prB(tab, 4, 2, 3); std::cout << "\n";
	std::cout << "4A  "; prB(tab, 1, 3, 2); std::cout << "     "; prB(tab, 2, 3, 3); std::cout << "     "; prB(tab, 3, 3, 4); std::cout << "  "; prB(tab, 3, 2, 1); std::cout << "  D  "; prB(tab, 4, 2, 2); std::cout << "\n";
	std::cout << " "; prB(tab, 1, 4, 4); std::cout << "  "; prB(tab, 1, 3, 1); std::cout << "  B  "; prB(tab, 2, 3, 2); std::cout << "     "; prB(tab, 3, 3, 3); std::cout << "     "; prB(tab, 4, 3, 4); std::cout << "  "; prB(tab, 4, 2, 1); std::cout << "\n";
	std::cout << " "; prB(tab, 1, 4, 3); std::cout << "     "; prB(tab, 2, 4, 4); std::cout << "  "; prB(tab, 2, 3, 1); std::cout << "  C  "; prB(tab, 3, 3, 2); std::cout << "     "; prB(tab, 4, 3, 3); std::cout << "\n";
	std::cout << " "; prB(tab, 1, 4, 2); std::cout << "     "; prB(tab, 2, 4, 3); std::cout << "     "; prB(tab, 3, 4, 4); std::cout << "  "; prB(tab, 3, 3, 1); std::cout << "  D  "; prB(tab, 4, 3, 2); std::cout << "\n";
	std::cout << " "; prB(tab, 1, 4, 1); std::cout << "     "; prB(tab, 2, 4, 2); std::cout << "     "; prB(tab, 3, 4, 3); std::cout << "     "; prB(tab, 4, 4, 4); std::cout << "  "; prB(tab, 4, 3, 1); std::cout << "\n";
	std::cout << "       "; prB(tab, 2, 4, 1); std::cout << "     "; prB(tab, 3, 4, 2); std::cout << "     "; prB(tab, 4, 4, 3); std::cout << "\n";
	std::cout << "             "; prB(tab, 3, 4, 1); std::cout << "     "; prB(tab, 4, 4, 2); std::cout << "\n";
	std::cout << "                   "; prB(tab, 4, 4, 1); std::cout << "\n";

}



bool check_win(Beads*** tab, int d) {
	Beads t;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			t = tab[0][i][j]; //dlugosci
			if (t != Beads::blank && t == tab[1][i][j] && t == tab[2][i][j] && t == tab[3][i][j]) {
				return true;
			}

			t = tab[i][0][j]; //szerokosci
			if (t != Beads::blank && t == tab[i][1][j] && t == tab[i][2][j] && t == tab[i][3][j]) {
				return true;
			}

			t = tab[i][j][0]; //wysokosci
			if (t != Beads::blank && t == tab[i][j][1] && t == tab[i][j][2] && t == tab[i][j][3]) {
				return true;
			}


		}
		t = tab[0][i][0];
		if (t != Beads::blank && t == tab[1][i][1] && t == tab[2][i][2] && t == tab[3][i][3]) {
			return true;
		}

		t = tab[i][0][0];
		if (t != Beads::blank && t == tab[i][1][1] && t == tab[i][2][2] && t == tab[i][3][3]) {
			return true;
		}

		t = tab[3][i][0];
		if (t != Beads::blank && t == tab[2][i][1] && t == tab[1][i][2] && t == tab[0][i][3]) {
			return true;
		}

		t = tab[i][3][0];
		if (t != Beads::blank && t == tab[i][2][1] && t == tab[i][1][2] && t == tab[i][0][3]) {
			return true;
		}
	}

	t = tab[0][0][0]; //wielka przekatna 1 
	if (t != Beads::blank && t == tab[1][1][1] && t == tab[2][2][2] && t == tab[3][3][3]) {
		return true;
	}
	 
	t = tab[3][0][0];
	if (t != Beads::blank && t == tab[2][1][1] && t == tab[1][2][2] && t == tab[0][3][3]) {
		return true;
	}

	return false;
}

int main()
{
	int d = 4;
	int x, y, h;
	bool valid_target;
	Beads player = Beads::red;
	
	

	Beads*** tab = new Beads ** [d];
	for (int i = 0; i < d; i++) {
		tab[i] = new Beads * [d];
		for (int j = 0; j < d; j++) {
			tab[i][j] = new Beads[d];
			for (int k = 0; k < d; k++) {
				tab[i][j][k] = Beads::blank;
			}
		}
	}
	print_board(tab, d);

	std::cout << "Score Four\n Nacisnij ENTER aby kontynuowac\n";
	
	
	
	while (!check_win(tab, d)) {
		do {
			valid_target = false;
			do {
				std::cin.clear();
				std::cin.ignore(INT_MAX, '\n');
				std::cout << "Tura gracza: ";
					if (player == Beads::red) {
						std::cout << "RED";
					}
					else std::cout << "YELLOW";

				std::cout << "\nPodaj cel: ";
				char temp;
				std::cin >> temp >> y;
				x = toupper(temp) - 'A'; y--;
			} while (!std::cin.good());
			if (0 <= x && x < 4 && 0 <= y && y < 4 && tab[x][y][3] == Beads::blank) valid_target = true;
		} while (!valid_target);
		h = 0;
		while (tab[x][y][h] != Beads::blank) {
			h++;
		}
		tab[x][y][h] = player;
		if (h == 3) {
			bool is_draw = true;
			for (int i = 0; i < 16; i++) {
				if (tab[i / 4][i % 4][3] != Beads::blank) {
					is_draw = false;
					break;
				}
			}
			if (is_draw) {
				std::cout << "\nDRAW\n";
				return 0;
			}
		}
		if (player == Beads::red) player = Beads::yellow;
		else player = Beads::red;
		print_board(tab, d);
		
	}

	return 0;
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

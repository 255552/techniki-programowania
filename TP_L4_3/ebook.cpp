#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include "ebook.h"

using namespace std;

void e_help()
{
    cout << "Książka adresowa\n"
         << "Obsługiwane komendy:\n"
         << "\t HELP \t wyświetla tę pomoc\n"
         << "\t INSERT \t dodaje element do książki adresowej (INSERT bob bob321@smail.com)\n"
         << "\t AT \t odczytuje element z książki adresowej (AT bob) => (bob : bob321@smail.com\n"
         << "\t EMPLACE \t zmienia wartość klucza w książce adresowej (EMPLACE bob bob456@smail.com)\n"
         << "\t ERASE \t usuwa element z książkie adresowej (ERASE bob)\n"
         << "\t CLEAR \t usuwa WSZYSTKIE elementy z książki adresowej\n"
         << "\t PRINT \t wypisuje na ekran wszystkie elementy książki adresowej\n"
         << "\t EXIT \t kończy pracę programu (program zapisze książkę do pliku \"data.csv\", który odczyta przy kolejnym uruchomieniu)\n\n";
}

bool e_insert(map<string, string> &ebook)
{
    string name, email;
    cin >> name >> email;
    //cout << name << " -- " << email << "\n";
    size_t at = email.find_first_of('@');
    size_t dot = email.find_last_of('.');
    if (at != string::npos && at < dot)
    {
        ebook.insert({name, email});
        if (ebook[name] != email)
        {
            cout << "Nie udało się dodać rekordu, prawdopodobnie w bazie znajduje się już klucz '"
                 << name << "', spróbuj użyć komendy EMPLACE.\n";
            return false;
        }
        cout << "Pomyślnie dodano " << name << " : " << ebook[name] << "\n";
        return true;
    }
    else
    {
        cout << "Spróbuj ponownie podając prawidłowy email (np. danhouser@email.com)\n";
        return false;
    }
}

bool e_at(map<string, string> &ebook)
{
    string name, email;
    cin >> name;
    try
    {
        email = ebook.at(name);
        cout << name << " : " << email << "\n";
        return true;
    }
    catch (const std::out_of_range &oor)
    {
        cout << "W bazie nie ma elementu o takim kluczu, sprawdź literówki, lub spróbuj dodać element (INSERT)\n";
        return false;
    }
}

bool e_emplace(map<string, string> &ebook)
{
    string name, email;
    cin >> name >> email;
    size_t at = email.find_first_of('@');
    size_t dot = email.find_last_of('.');
    if (at != string::npos && at > dot)
    {
        ebook.emplace(name, email);
        if (ebook[name] != email)
        {
            cout << "Nie udało się zamienić rekordu\n";
            return false;
        }
        return true;
    }
    else
    {
        cout << "Spróbuj ponownie podając prawidłowy email (np. danhouser@email.com)\n";
        return false;
    }
}

bool e_erase(map<string, string> &ebook)
{
    string name, email;
    cin >> name;
    try
    {
        ebook.at(name);
        ebook.erase(name);
        cout << "Pomyślnie usunięto\n";
        return true;
    }
    catch (const std::out_of_range &oor)
    {
        cout << "W bazie nie ma elementu o takim kluczu, sprawdź literówki\n";
        return false;
    }
}

bool e_clear(map<string, string> &ebook)
{
    cout << "Czy na pewno chcesz usunąć WSZYSTKIE elementy z książki adresowej?\n [Y/n]\n";
    char odp;
    cin >> odp;
    if (odp == 'Y' || odp == 'y')
    {
        ebook.clear();
        cout << "Pomyślnie usunięto wszystkie elementy\n";
        return true;
    }
    return false;
}

bool e_print(map<string, string> &ebook)
{
    map<string, string>::iterator itr;
    for (itr = ebook.begin(); itr != ebook.end(); itr++)
    {
        cout << itr->first << " : " << itr->second << "\n";
    }
    return true;
}
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include "ebook.h"

using namespace std;

int main()
{
    fstream f;
    f.open("data.csv", std::fstream::in);

    if (!f)
    {
        cout << "Nie udało się otworzyć pliku\n";
        return -1;
    }

    map<string, string> ebook;

    string name, email;
    while (f >> name >> email)
    {
        ebook[name] = email;
    }
    f.close();

    e_help();

    string input;
    while (cin >> input)
    {
        transform(input.begin(), input.end(), input.begin(),
                  [](unsigned char c) { return std::tolower(c); }); // lowercase
        if (input == "help")
        {
            e_help();
            continue;
        }
        if (input == "insert")
        {
            e_insert(ebook);
            continue;
        }
        if (input == "emplace")
        {
            e_emplace(ebook);
            continue;
        }
        if (input == "at")
        {
            e_at(ebook);
            continue;
        }
        if (input == "erase")
        {
            e_erase(ebook);
            continue;
        }
        if (input == "clear")
        {
            e_clear(ebook);
            continue;
        }
        if (input == "print")
        {
            e_print(ebook);
            continue;
        }
        if (input == "exit")
        {
            break;
        }
        else
        {
            cout << "Nieprawidłowa komenda, użyj HELP aby uzyskać listę komend\n";
        }
    }

    f.open("data.csv", std::fstream::out | std::fstream::trunc);
    if (!f)
    {
        cout << "Nie udało się otworzyć pliku\n";
        return -1;
    }
    map<string, string>::iterator itr;
    for (itr = ebook.begin(); itr != ebook.end(); itr++)
    {
        f << itr->first << " " << itr->second << "\n";
    }
    cout << "Zapisano dane do pliku \"data.csv\"\n";

    f.close();
    return 0;
}
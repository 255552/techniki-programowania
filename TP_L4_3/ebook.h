#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include "ebook.h"

using namespace std;

void e_help();

// cout << "Książka adresowa\n"
//      << "Obsługiwane komendy:\n"
//      << "\t HELP \t wyświetla tę pomoc\n"
//      << "\t INSERT \t dodaje element do książki adresowej (INSERT bob bob321@smail.com)\n"
//      << "\t AT \t odczytuje element z książki adresowej (AT bob) => (bob : bob321@smail.com\n"
//      << "\t EMPLACE \t zmienia wartość klucza w książce adresowej (EMPLACE bob bob456@smail.com)\n"
//      << "\t ERASE \t usuwa element z książkie adresowej (ERASE bob)\n"
//      << "\t CLEAR \t usuwa WSZYSTKIE elementy z książki adresowej\n"
//      << "\t PRINT \t wypisuje na ekran wszystkie elementy książki adresowej\n"
//      << "\t EXIT \t kończy pracę programu (program zapisze książkę do pliku \"data.csv\", który odczyta przy kolejnym uruchomieniu)\n\n";

bool e_insert(map<string, string> &ebook);

bool e_at(map<string, string> &ebook);

bool e_emplace(map<string, string> &ebook);

bool e_erase(map<string, string> &ebook);

bool e_clear(map<string, string> &ebook);

bool e_print(map<string, string> &ebook);

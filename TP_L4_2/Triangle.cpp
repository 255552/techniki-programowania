#include "Triangle.h"

//Konstruktor bez argumentów - trójkąt o bokach 1,1,1
Triangle::Triangle()
    : _a(1),
      _b(1),
      _c(1)
{
}

//Konstruktor pobierający 3 boki
Triangle::Triangle(double a, double b, double c)
    : _a(a > 0 ? (a < b + c ? a : throw std::domain_error("Such triangle doesn't exist (a>=b+c)")) : throw std::domain_error("a must be >0")),
      _b(b > 0 ? (b < a + c ? a : throw std::domain_error("Such triangle doesn't exist (b>=a+c)")) : throw std::domain_error("b must be >0")),
      _c(c > 0 ? (c < a + b ? a : throw std::domain_error("Such triangle doesn't exist (c>=a+b)")) : throw std::domain_error("c must be >0"))
{
}

//Funkcja zwracająca pole
double Triangle::area() const
{
    double p = (this->_a + this->_b + this->_c) / 2;
    return std::sqrt(p * (p - this->_a) * (p - this->_b) * (p - this->_c));
}

//Wypisanie
std::ostream &Triangle::print(std::ostream &out) const
{
    out << "TRIANGLE with edges : ("
        << this->_a << ", "
        << this->_b << ", "
        << this->_c << ")\n";
    return out;
}
#pragma once

#include <iostream>
#include "Figure.h"

class Circle : public Figure
{
public:
    //Konstruktor bazowy - promień 1
    Circle();

    //Konstruktor pobierający promień
    Circle(double);

    //Destruktor domyślny
    // ~Circle();

    //Funkcja zwracająca pole
    double area() const;

    //Wypisanie
    std::ostream &print(std::ostream &) const;

private:
    //Wartość promienia
    double _r;
};
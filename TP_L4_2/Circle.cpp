#include "Circle.h"

#define M_PI 3.14159265358979323846 /* pi */

//Konstruktor bazowy - promień 1
Circle::Circle() : _r(1){};

//Konstruktor pobierający promień
Circle::Circle(double r) : _r(r >0 ? r : throw std::domain_error("R must be greater than 0!")){};

//Funkcja zwracająca pole
double Circle::area() const
{
    return M_PI * this->_r * this->_r;
}

//Wypisanie
std::ostream &Circle::print(std::ostream &out) const
{
    out << "CIRCLE with radius : (" << this->_r << ")\n";
    return out;
}
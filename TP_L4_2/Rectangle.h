#pragma once

#include <iostream>
#include "Figure.h"

class Rectangle : public Figure
{
public:
    //Konstruktor bez argumentów - kwadrat 1x1
    Rectangle();

    // Konstruktor pobierający 2 boki
    Rectangle(double, double);

    //Destruktor domyślny
    //~Triangle();

    //Funkcja zwracająca pole
    double area() const;

    //Wypisanie
    std::ostream &print(std::ostream &) const;

private:
    // Bok a
    double _a;
    //Bok b
    double _b;
};
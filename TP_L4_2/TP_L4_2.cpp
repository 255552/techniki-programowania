#include <iostream>
#include "Figure.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"

using namespace std;

int main()
{
    Figure **figures = new Figure *[3];
    figures[0] = new Circle(1);
    figures[1] = new Triangle(1, 1, 1);
    figures[2] = new Rectangle(2, 3);
    for (int i = 0; i < 3; i++)
    {
        cout << *figures[i] << endl;
    }
    cout << "Pola figur:" << endl;
    for (int i = 0; i < 3; i++)
    {
        cout << figures[i]->area() << endl;
    }
    for (int i = 0; i < 3; i++)
    {
        delete figures[i];
    }
    delete[] figures;

    // Figure **figures = new Figure *[3];
    // figures[0] = new Circle(1);

    // Circle cir = Circle(-1); //Promień musi być dodatni
    // cir.print(std::cout);
    // cout << cir;
    // Triangle trg = Triangle(3, 1, 4); //Trójkąt nie istnieje
    // std::cout << trg.area() << std::endl;

    // Rectangle rctgl = Rectangle(3, -2); //Boki muszą być >0
    // cout << rctgl;
    // cout << rctgl.area() << std::endl;

    return 0;
}
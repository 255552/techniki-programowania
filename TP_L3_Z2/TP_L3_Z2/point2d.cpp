/*point2d.cpp*/
/*Plik �r�d�owy klasy Point2d.hpp*/
#include<iostream>
#include<cmath>
#include "point2d.hpp"
#define M_PI 4*atan(1.)


//konstruktor bez argument�w zwraca punkt (0,0)
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d()
    : _R(0), _Phi(0)
{
}

//konstruktor pobieraj�cy wsp�rz�dne
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(double x, double y)
    : _R(sqrt(x * x + y * y))
    , _Phi(atan2(y, x)) {
}

//konstruktor kopiuj�cy
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(const Point2d& other)
    : _R(other._R)
    , _Phi(other._Phi) {
}

//przeci��ony operator przypisania
Point2d& Point2d::operator= (const Point2d& other) {
    //wykorzystano wska�nik this pokazuj�cy
    //na "ten" obiekt
    this->_R = other._R;
    this->_Phi = other._Phi;
    //operator zwraca "ten" obiekt, aby mo�na by�o
    //wykona� wielokrotne przypisanie
    return *this;
}

//wsp�rz�dna x
double Point2d::getX() {
    return _R * cos(_Phi);
}

//wsp�rz�dna y
double Point2d::getY() {
    return _R * sin(_Phi);
}

//wsp�rz�dna r
double Point2d::getR() {
    return _R;
}

//wsp�rz�dna phi
double Point2d::getPhi() {
    return _Phi;
}

void Point2d::setXY(double x, double y) {
    //przypisanie z wykorzystaniem wska�nika this oraz wprost
    _R = sqrt(x * x + y * y);
    this->_Phi = atan2(y, x);
}

void Point2d::setRPhi(double r, double phi) {
    _R = r;
    _Phi =  phi;
}

Point2d Point2d::homothety(double k) {
    Point2d::setRPhi(this->getR() * k, this->getPhi());
    return *this;
}

Point2d Point2d::rotation(double theta) {
    Point2d::setRPhi(this->getR(), this->getPhi() + theta);
    return *this;
}

//przeci��ony operator<< dla wypisywania
std::ostream& operator<<(std::ostream& out, Point2d& p) {
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
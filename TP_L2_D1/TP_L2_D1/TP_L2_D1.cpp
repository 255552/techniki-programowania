﻿// TP_L2_D1.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <Windows.h>
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

enum class Beads { red, yellow, blank };
void print_board(Beads** tab, int x, int y);

bool check_win(Beads** tab, int x, int y);

void del_tab(Beads** tab, int x);


int main()
{
	int x = 7;
	int y = 6;
	int target;
	bool valid_target;
	int h = 0;
	Beads player = Beads::red;

	std::cout << "Connect four\nPodaj rozmiar planszy - szerokosc, wysokosc - 2 liczby oddzielone spacja:";
	std::cin >> x >> y;

	
	Beads** tab = new Beads * [x];
	for (int i = 0; i < x; i++) {
		tab[i] = new Beads[y];
		for (int j = 0; j < y; j++) {
			tab[i][j] = Beads::blank;
		}
	}
	print_board(tab, x, y);

	while (!check_win(tab, x, y)) {
		do {
			valid_target = false;
			do {
				std::cin.clear();
				std::cin.ignore(INT_MAX, '\n');
				std::cout << "Tura gracza: ";
				if (player == Beads::red) {
					std::cout << "RED";
				}
				else
				{
					std::cout << "YELLOW";
				}
					
				std::cout << "\nPodaj cel : ";
				std::cin >> target;
			} while (!std::cin.good());
			target--;
			if (0 <= target && target < x && tab[target][y - 1] == Beads::blank) valid_target = true;
		} while (!valid_target);

		h = 0;
		while (tab[target][h] != Beads::blank) {
			h++;
		}
		tab[target][h] = player;
		if (player == Beads::red) {

			player = Beads::yellow;
		}
		else {
			player = Beads::red;
		}
		print_board(tab, x, y);
		h = 0;
		while (tab[h][y - 1] != Beads::blank) {
			if (h == x - 1 && !check_win(tab,x,y)) {
				std::cout << "DRAW\n";
				del_tab(tab, x);
				return 0;
			}
			h++;
		}
	}

	//ostatni ruch zrobił gracz żółty i zmienił się kolor na przeciwny
	if (player == Beads::red) std::cout << "YELLOW "; 
	else std::cout << "RED ";
	std::cout << "player wins!\nCongratulations!\n";

	

	return 0;
}

void print_board(Beads** tab, int x, int y) {
	system("cls");
	for (int i = y - 1; i >= 0; i--) {
		std::cout << (char)186;
		for (int j = 0; j < x; j++) {
			switch (tab[j][i]) {
			case Beads::blank:
				if ((i + j) % 2)std::cout << (char)176;
				else std::cout << (char)177;
				break;
			case Beads::red:SetConsoleTextAttribute(hConsole, 4);
				std::cout << (char)254;
				break;
			case Beads::yellow:SetConsoleTextAttribute(hConsole, 6);
				std::cout << (char)254;
				break;
			}
			SetConsoleTextAttribute(hConsole, 7);
		}
		std::cout << (char)186 << "\n";
	}
	std::cout << (char)200;
	for (int i = 1; i <= x; i++) {
		std::cout << i;
	}
	std::cout << (char)188 << "\n";
}

bool check_win(Beads** tab, int x, int y) {
	//mechanizm sprawdzania wygranej
	for (int i = 0; i < x; i++) {
		for (int j = 0; j < y; j++) {
			if (tab[i][j] != Beads::blank) {
				if (i + 3 < x
					&& tab[i][j] == tab[i + 1][j]
					&& tab[i][j] == tab[i + 2][j]
					&& tab[i][j] == tab[i + 3][j]) {
					return true;
				}
				if (i + 3 < x && j + 3 < y
					&& tab[i][j] == tab[i + 1][j + 1]
					&& tab[i][j] == tab[i + 2][j + 2]
					&& tab[i][j] == tab[i + 3][j + 3]) {
					return true;
				}
				if (j + 3 < y
					&& tab[i][j] == tab[i][j + 1]
					&& tab[i][j] == tab[i][j + 2]
					&& tab[i][j] == tab[i][j + 3]) {
					return true;
				}
				if (i - 3 >= 0 && j + 3 < y
					&& tab[i][j] == tab[i - 1][j + 1]
					&& tab[i][j] == tab[i - 2][j + 2]
					&& tab[i][j] == tab[i - 3][j + 3]) {
					return true;
				}
			}
		}
	}
	return false;
}

void del_tab(Beads** tab, int x) {
	for (int i = 0; i < x; i++) {
		delete[] tab[i];
	}
	delete[] tab;
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

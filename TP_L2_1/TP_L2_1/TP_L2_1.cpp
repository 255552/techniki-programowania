﻿// TP_L2_1.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
enum class Symbol { X, O, blank };
void print_board(Symbol tab[3][3]);
bool check_win(Symbol tab[3][3]);

int main()
{
    int a, b;//punkty strzałów
    Symbol player = Symbol::X; //który gracz
    bool valid_move;
    //deklaracja pustej tablicy
    Symbol tab[3][3];
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            tab[i][j] = Symbol::blank;
        }
    }
    print_board(tab);

    do
    {
        if (player==Symbol::X) std::cout << "Tura gracza: X" << std::endl;
        else std::cout << "Tura gracza: O" << std::endl;
        do
        {
            valid_move = false;

            std::cout << "Podaj pole (dwie liczby oddzielone spacja, pierwsza wybierz rzad, a druga kolumne):";
            std::cin >> a >> b;
            a--; b--;
            if (a >= 0 && a <= 2 && b >= 0 && b <= 2)
            {
                if (tab[a][b] == Symbol::blank)
                {
                    tab[a][b] = player;
                    if (player == Symbol::X)
                    {
                        player = Symbol::O;
                    }
                    else
                    {
                        player = Symbol::X;
                    }
                    valid_move = true;
                }
                else
                {
                    std::cout << "To miejsce jest juz zajete" << std::endl;
                }
            }
            else
            {
                std::cout << "Podaj poprawne wspolrzedne" << std::endl;
            }
            system("cls");
            print_board(tab);
        } while (!valid_move);
        std::cout << std::endl;
    } while (!check_win(tab));
    std::cout << "Wygral gracz: ";
    if (player==Symbol::O) std::cout << "X";
    else std::cout << "O";
    std::cout << std::endl << "Gratulacje!" << std::endl;



    return 0;
}

bool check_win(Symbol tab[3][3])
{
    if (tab[1][1] != Symbol::blank)
    {
        if (tab[0][0] == tab[1][1] && tab[1][1] == tab[2][2])
            return true;
        if (tab[0][2] == tab[1][1] && tab[1][1] == tab[2][0])
            return true;
        if (tab[0][1] == tab[1][1] && tab[1][1] == tab[2][1])
            return true;
        if (tab[1][0] == tab[1][1] && tab[1][1] == tab[1][2])
            return true;
    }
    if (tab[0][0] != Symbol::blank)
    {
        if (tab[0][0] == tab[0][1] && tab[0][0] == tab[0][2])
            return true;
        if (tab[0][0] == tab[1][0] && tab[0][0] == tab[2][0])
            return true;
    }
    if (tab[2][2] != Symbol::blank)
    {
        if (tab[2][0] == tab[2][1] && tab[2][1] == tab[2][2])
            return true;
        if (tab[0][2] == tab[1][2] && tab[1][2] == tab[2][2])
            return true;
    }
    return false;
}

void print_board(Symbol tab[3][3])
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            switch (tab[i][j])
            {
            case Symbol::blank:
                std::cout << (char)254;
                break;
            case Symbol::X:
                std::cout << "X";
                break;
            case Symbol::O:
                std::cout << "O";
                break;
            }
        }
        std::cout << std::endl;
    }
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

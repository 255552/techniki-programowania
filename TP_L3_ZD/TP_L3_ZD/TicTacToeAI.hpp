#pragma once
/*TicTacToeAI.hpp*/
/*Plik nag��wkowy klasy TicTacToe.hpp*/
#ifndef TICTACTOEAI_HPP_INCLUDED
#define TICTACTOEAI_HPP_INCLUDED
#include <iostream>
#include <vector>
#include <algorithm>

enum class Beads{
    BLANK,
    X,
    O
};
std::ostream& operator<<(std::ostream& out, Beads b);

struct AImove
{
    AImove() {}
    AImove(int score) : score(score) {}
    size_t x;
    size_t y;
    int score;
};

class TicTacToe {
public:
    //konstruktor bez argument�w tworzy pust� plansz� z zaczynaj�cym X, oraz zwyci�zc� BLANK
    TicTacToe();

    //destruktor nie jest zadeklarowany
    //b�dzie wykorzystywany domy�lny
    //~TicTacToe();

    bool setBead(int, int);
    bool setBead(int, int, Beads);
    Beads getBead(int, int);
    //Beads** getBoard();
    void printBoard();


    Beads getPlayer();
    Beads flipPlayer();

    bool check_draw();

    bool check_win();
    Beads the_Winner_is();
    Beads getWinner();
    void setWinner(Beads);

    int score(int depth);
    AImove minmax(int depth);
    void perform_move(AImove);


private:
    //wsp�rz�dne punktu jako pola prywatne
    Beads _board[3][3];
    Beads _player;
    Beads _winner;
};

//deklaracja przeci��onego operatora <<
//na potrzeby wy�wietlania wsp�rz�dnych punktu
//std::ostream& operator<<(std::ostream& out, Point2d& p);

#endif // TICTACTOEAI_HPP_INCLUDED


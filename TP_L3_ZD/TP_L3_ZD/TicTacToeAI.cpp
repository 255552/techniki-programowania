#pragma once
/*TicTacToeAI.cpp*/
/*Plik �r�d�owy klasy TicTacToe.hpp*/
#include <iostream>
#include <vector>
#include <algorithm>
#include "TicTacToeAI.hpp"

//konstruktor bez argument�w tworzy pust� plansz� z zaczynaj�cym X, oraz zwyci�zc� BLANK
TicTacToe::TicTacToe() :
    _board{ {Beads::BLANK, Beads::BLANK, Beads::BLANK}, {Beads::BLANK, Beads::BLANK, Beads::BLANK}, {Beads::BLANK, Beads::BLANK, Beads::BLANK} }
    , _player{ Beads::X }
    , _winner{ Beads::BLANK }{
    //   //_board[3][3];
       //for (int i = 0; i < 3; i++) {
       //	for (int j = 0; j < 3; j++) {
       //		_board[i][j] = Beads::BLANK;
       //	}
       //}
       //_player = Beads::X;
       //_winner = Beads::BLANK;
}

Beads TicTacToe::getBead(int x, int y) {
    if (0 <= x && x < 3 && 0 <= y && y < 3) {
        return _board[x][y];
    }
    throw "Chcesz odczyta� pole znajduj�ce si� poza plansz�!";
}

bool TicTacToe::setBead(int x, int y) {
    if (0 <= x && x < 3 && 0 <= y && y < 3 && getBead(x,y)==Beads::BLANK) {
        _board[x][y] = _player;
        //this->flipPlayer();
        return true;
    }
    return false;
}


bool TicTacToe::setBead(int x, int y, Beads b) {
    if (0 <= x && x < 3 && 0 <= y && y < 3) {
        _board[x][y] = b;
        //this->flipPlayer();
        return true;
    }
    return false;
}

//Beads** TicTacToe::getBoard() {
//    return _board;
//}

void TicTacToe::printBoard()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            switch (this->getBead(i,j))
            {
            case Beads::BLANK:
                std::cout << (char)254;
                break;
            case Beads::X:
                std::cout << "X";
                break;
            case Beads::O:
                std::cout << "O";
                break;
            }
        }
        std::cout << std::endl;
    }
}

Beads TicTacToe::getPlayer() {
    return _player;
}

Beads TicTacToe::flipPlayer() {
    if (_player == Beads::X) _player = Beads::O;
    else _player = Beads::X;
    return _player;
}

Beads TicTacToe::getWinner() {
    return _winner;
}

void TicTacToe::setWinner(Beads new_winner) {
    _winner = new_winner;
}

bool TicTacToe::check_win() {
    if (this->the_Winner_is() != Beads::BLANK)return true;
    else return false;
}

Beads TicTacToe::the_Winner_is()
{ 
    if (_board[1][1] != Beads::BLANK)
    {
        if (_board[0][0] == _board[1][1] && _board[1][1] == _board[2][2])
            return _board[1][1];
        if (_board[0][2] == _board[1][1] && _board[1][1] == _board[2][0])
            return _board[1][1];
        if (_board[0][1] == _board[1][1] && _board[1][1] == _board[2][1])
            return _board[1][1];
        if (_board[1][0] == _board[1][1] && _board[1][1] == _board[1][2])
            return _board[1][1];
    }
    if (_board[0][0] != Beads::BLANK)
    {
        if (_board[0][0] == _board[0][1] && _board[0][0] == _board[0][2])
            return _board[0][0];
        if (_board[0][0] == _board[1][0] && _board[0][0] == _board[2][0])
            return _board[0][0];
    }
    if (_board[2][2] != Beads::BLANK)
    {
        if (_board[2][0] == _board[2][1] && _board[2][1] == _board[2][2])
            return _board[0][0];
        if (_board[0][2] == _board[1][2] && _board[1][2] == _board[2][2])
            return _board[0][0];
    }
    return Beads::BLANK;
}

bool TicTacToe::check_draw() {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (this->getBead(i, j) == Beads::BLANK) return false;
        }
    }
    if (this->check_win()) return false;
    return true;
}

int TicTacToe::score(int depth) {
    if (_winner == Beads::X) {
        return 10 - depth;
    }
    else if (_winner == Beads::O) {
        return -10 + depth;
    }
    else return 0;
}

AImove TicTacToe::minmax(int depth) {
    _winner = Beads::BLANK;
    if (this->check_draw() || this->check_win()) {
        this->setWinner(this->the_Winner_is());
        return score(depth);
    }
    ++depth;

    //std::vector<int> scores;
    std::vector<AImove> moves;



    for (size_t x = 0; x < 3; x++) {
        for (size_t y = 0; y < 3; y++) {
            if (this->getBead(x,y) == Beads::BLANK) {
                this->setBead(x, y);
                this->flipPlayer();
                
                AImove move;

                move.x = x;
                move.y = y;
                move.score = minmax(depth).score;

                moves.push_back(move);

                this->flipPlayer();
                this->setBead(x, y, Beads::BLANK);
            }
        }
    }

    int best_move = 0;
    if (_player == Beads::X) {
        //MAX calc
        int best_score = -100;
        AImove bestmove;
        for (size_t i = 0; i < moves.size();i++) {
            if (moves[i].score > best_score) {
                best_score = moves[i].score;
                best_move = i;
            }
        }
    }
    if (_player == Beads::O) {
        //MAX calc
        int best_score = 100;
        AImove bestmove;
        for (size_t i = 0; i < moves.size(); i++) {
            if (moves[i].score < best_score) {
                best_score = moves[i].score;
                best_move = i;
            }
        }
    }
    return moves[best_move];
}

void TicTacToe::perform_move(AImove move) {
    this->setBead(move.x, move.y);
}



std::ostream& operator<<(std::ostream& out, Beads b)
{
    switch (b)
    {
    case Beads::BLANK:
        out << "BLANK";
        break;
    case Beads::X:
        out << "X";
        break;
    case Beads::O:
        out << "O";
        break;
    default:
        out << "error";
    }
    return out;
}

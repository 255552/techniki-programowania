#pragma once
/*TicTacToe.cpp*/
/*Plik �r�d�owy klasy TicTacToe.hpp*/
#include <iostream>
#include "TicTacToe.hpp"

//konstruktor bez argument�w tworzy pust� plansz� z zaczynaj�cym X, oraz zwyci�zc� BLANK
TicTacToe::TicTacToe() :
    _board{ {Beads::BLANK, Beads::BLANK, Beads::BLANK}, {Beads::BLANK, Beads::BLANK, Beads::BLANK}, {Beads::BLANK, Beads::BLANK, Beads::BLANK} }
    , _player{Beads::X}
    , _winner{ Beads::BLANK }{
 //   //_board[3][3];
	//for (int i = 0; i < 3; i++) {
	//	for (int j = 0; j < 3; j++) {
	//		_board[i][j] = Beads::BLANK;
	//	}
	//}
	//_player = Beads::X;
	//_winner = Beads::BLANK;
}

Beads TicTacToe::getBead(int x, int y) {
    if (0 <= x && x < 3 && 0 <= y && y < 3) {
        return _board[x][y];
    }
    throw "Chcesz odczyta� pole znajduj�ce si� poza plansz�!";
}

bool TicTacToe::setBead(int x, int y) {
    if (0 <= x && x < 3 && 0 <= y && y < 3 && getBead(x,y)==Beads::BLANK) {
        _board[x][y] = _player;
        this->flipPlayer();
        return true;
    }
    return false;
}

//Beads** TicTacToe::getBoard() {
//    return _board;
//}

void TicTacToe::printBoard()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            switch (this->getBead(i,j))
            {
            case Beads::BLANK:
                std::cout << (char)254;
                break;
            case Beads::X:
                std::cout << "X";
                break;
            case Beads::O:
                std::cout << "O";
                break;
            }
        }
        std::cout << std::endl;
    }
}

Beads TicTacToe::getPlayer() {
    return _player;
}

Beads TicTacToe::flipPlayer() {
    if (_player == Beads::X) _player = Beads::O;
    else _player = Beads::X;
    return _player;
}

Beads TicTacToe::getWinner() {
    return _winner;
}

void TicTacToe::setWinner(Beads new_winner) {
    _winner = new_winner;
}

bool TicTacToe::check_win()
{ 
    if (_board[1][1] != Beads::BLANK)
    {
        if (_board[0][0] == _board[1][1] && _board[1][1] == _board[2][2])
            return true;
        if (_board[0][2] == _board[1][1] && _board[1][1] == _board[2][0])
            return true;
        if (_board[0][1] == _board[1][1] && _board[1][1] == _board[2][1])
            return true;
        if (_board[1][0] == _board[1][1] && _board[1][1] == _board[1][2])
            return true;
    }
    if (_board[0][0] != Beads::BLANK)
    {
        if (_board[0][0] == _board[0][1] && _board[0][0] == _board[0][2])
            return true;
        if (_board[0][0] == _board[1][0] && _board[0][0] == _board[2][0])
            return true;
    }
    if (_board[2][2] != Beads::BLANK)
    {
        if (_board[2][0] == _board[2][1] && _board[2][1] == _board[2][2])
            return true;
        if (_board[0][2] == _board[1][2] && _board[1][2] == _board[2][2])
            return true;
    }
    return false;
}

Beads TicTacToe::the_Winner_is() {
    if (this->check_win() == true) {
        return this->flipPlayer();
    }
    else return Beads::BLANK;
}

bool TicTacToe::check_draw() {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (this->getBead(i, j) == Beads::BLANK) return false;
        }
    }
    if (this->check_win()) return false;
    return true;
}

std::ostream& operator<<(std::ostream& out, Beads b)
{
    // TODO: tu wstawi� instrukcj� return
    switch (b)
    {
    case Beads::BLANK:
        out << "BLANK";
        break;
    case Beads::X:
        out << "X";
        break;
    case Beads::O:
        out << "O";
        break;
    default:
        out << "error";
    }
    return out;
}

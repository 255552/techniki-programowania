﻿// TP_L3_Z3.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <stdlib.h>
#include "TicTacToe.hpp"

using namespace std;

int main()
{
    TicTacToe TTT = TicTacToe();
    int x, y;
    while (!TTT.check_win() && !TTT.check_draw()) {
        system("cls");
        TTT.printBoard();
        cout << "Tura gracza: " << TTT.getPlayer() << "\n(podaj dwie liczby z zakresu [1,2,3] oddzielone spacja)\n";
        cin >> x >> y;
        while (!TTT.setBead(--x, --y)) {
            cout << "Pole jest juz zajete lub znajduje sie poza plansza, wykonaj poprawny ruch\n";
            cin >> x >> y;
        }
    }

    system("cls");
    TTT.printBoard();

    TTT.setWinner(TTT.the_Winner_is());
    if (TTT.getWinner() == Beads::BLANK && TTT.check_draw()) {
        cout << "Gra zakonczya sie remisem\n";
        return 0;
    }
    else {
        cout << "Wygral gracz: " << TTT.getWinner() << "\nGratualcje!\n";
        return 0;
    }
    cout << "Something went wrong\n";
    return 1;
}
    

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

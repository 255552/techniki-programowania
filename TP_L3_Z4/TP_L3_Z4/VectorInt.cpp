#pragma once
/*VectorInt.cpp*/
/*Plik �r�d�owy klasy VectorInt.hpp*/
#include <iostream>
#include "VectorInt.hpp"

//konstruktor bez argument�w alokuje pami�� na 16 liczb ca�kowitych
VectorInt::VectorInt() {
	_arr = new int[16];
	_capacity = 16;
	_current = 0;
}

//konstruktor przyjmuj�cy jeden argument - liczb� element�w, na kt�re powinna wystarczy� pierwotna alokacja pami�ci
VectorInt::VectorInt(int capacity) {
	_arr = new int[capacity];
	_capacity = capacity;
	_current = 0;
}

//konstruktor kopiuj�cy
VectorInt::VectorInt(const VectorInt& other) {
	_capacity = other._capacity;
	_current = other._current;
	_arr = new int[_capacity];
	for (size_t i = 0; i < _current; i++) {
		_arr[i] = other._arr[i];
	}
}

//operator przypisania
VectorInt& VectorInt::operator= (const VectorInt& other) {
	if (this == &other) {
		return *this;
	}
	delete[] _arr;
	_capacity = other._capacity;
	_current = other._current;
	_arr = new int[_capacity];
	for (size_t i = 0; i < _current; i++) {
		_arr[i] = other._arr[i];
	}
	return *this;
}

//destruktor
VectorInt::~VectorInt() {
	delete[] _arr;
	_arr = nullptr;
	_capacity = 0;
	_current = 0;
}

//at przyjmuje indeks i zwraca warto�� na tej pozycji
int VectorInt::at(size_t i) {
	if (0 <= i && i < _current) {
		return _arr[i];
	}
	else {
		//throw "Out of index";
		return NULL;
	}
}

//insert przyjmuje indeks i warto��, kt�r� wstawia na tej pozycji
void VectorInt::insert(size_t i, int x) {
	if (0 <= i && i < _current) {
		_arr[i] = x;
	}
	if (i == _current) {
		this->pushBack(x);
	}
	else {
		//throw "Out of index";
	}
}

//pushBack dodaje wskazany element na ko�cu ci�gu (w razie potrzeby powinno nas�pi� powi�kszenie pami�ci)
void VectorInt::pushBack(int x) {
	if (_current >= _capacity) {
		size_t old_cap = _capacity;
		_capacity *= 2;
		int* arr1 = new int[_capacity];
		for (size_t i = 0; i < old_cap; i++) {
			arr1[i] = _arr[i];
		}
		delete[] _arr;
		_arr = arr1;
	}
	_arr[_current++] = x;
}

//popBack usuwa ostatni element ci�gu
void VectorInt::popBack() {
	if (_current > 0) {
		//throw "Wektor jest pusty!";
		--_current;
	}
	else {
		//throw "Wektor jest pusty!";
	}
}

//shrinkToFit dostosowuje zaalokowan� pami�� do jej wype�nienia
void VectorInt::shrinkToFit() {
	int* arr1 = new int[_current];
	for (size_t i = 0; i < _current; i++) {
		arr1[i] = _arr[i];
	}
	delete[] _arr;
	_arr = arr1;
	_capacity = _current;
}

//clear pozwala na usuni�cie wszystkich element�w wektora
void VectorInt::clear() {
	delete[] _arr;
	_arr = nullptr;
	_capacity = 0;
	_current = 0;
}

//size zwraca liczb� element�w przechowywanych w wektorze
size_t VectorInt::size() {
	return _current;
}

//capacity zwraca liczb� elemetn�w, kt�re mo�na przechowywa� w wektorze bez relokacji pami�ci
size_t VectorInt::capacity() {
	return _capacity;
}

//przeci��ony operator << wypisuj�cy elementy wektora
std::ostream& operator<<(std::ostream& out, VectorInt& v) {
	out << "[ ";
	for (size_t i = 0; i < v.size(); i++) {
		out << v.at(i) << " ";
	}
	out << "]";
	return out;
}
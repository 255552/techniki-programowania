﻿// TP_L3_Z4.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include "VectorInt.hpp"

using namespace std;

int main()
{
    VectorInt vec(2);
    
    vec.pushBack(7);
    //cout << vec;
    //vec.popBack();
    //cout << vec;
    VectorInt vec2(vec);
    vec.popBack();
    vec.popBack();
    vec.popBack();

    vec2.pushBack(9);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.pushBack(3);
    vec2.insert(0, vec2.capacity());
    
    vec2.shrinkToFit();

    VectorInt vec3 = vec;
    cout << vec3 << "\n";
    vec3.pushBack(333);
    cout << vec3 << "\n";
    vec3 = vec;
    cout << vec3 << "\n";
    cout << vec3.at(7) << "\n";
    vec3.clear();


    cout << vec.capacity() << vec << vec2.capacity() << vec2;

}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

#pragma once
/*VectorInt.hpp*/
/*Plik nag��wkowy klasy VectorInt.hpp*/

class VectorInt
{
public:
	//konstruktor bez argument�w alokuje pami�� na 16 liczb ca�kowitych
	VectorInt();

	//konstruktor przyjmuj�cy jeden argument - liczb� element�w, na kt�re powinna wystarczy� pierwotna alokacja pami�ci
	VectorInt(int);

	//konstruktor kopiuj�cy
	VectorInt(const VectorInt& other);

	//operator przypisania
	VectorInt& operator= (const VectorInt& other);

	//destruktor
	~VectorInt();

	//at przyjmuje indeks i zwraca warto�� na tej pozycji
	int at(size_t);

	//insert przyjmuje indeks i warto��, kt�r� wstawia na tej pozycji
	void insert(size_t, int);

	//pushBack dodaje wskazany element na ko�cu ci�gu (w razie potrzeby powinno nas�pi� powi�kszenie pami�ci)
	void pushBack(int);

	//popBack usuwa ostatni element ci�gu
	void popBack();

	//shrinkToFit dostosowuje zaalokowan� pami�� do jej wype�nienia
	void shrinkToFit();

	//clear pozwala na usuni�cie wszystkich element�w wektora
	void clear();

	//size zwraca liczb� element�w przechowywanych w wektorze
	size_t size();

	//capacity zwraca liczb� elemetn�w, kt�re mo�na przechowywa� w wektorze bez relokacji pami�ci
	size_t capacity();
	


private:
	int* _arr;
	size_t _capacity;
	size_t _current;
};

std::ostream& operator<<(std::ostream& out, VectorInt& v);

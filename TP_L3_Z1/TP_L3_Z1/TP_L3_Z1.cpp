﻿// TP_L3_Z1.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool check_char(char p, char c) {
    if (p == c) return true;
    if (p == '?') return true;
    else return false;
}

bool check_patt(const string& patt, const string& s, size_t pos = 0) {
    if (patt.length() > s.length() - pos) return false;
    //if (patt.length()==0) return true;
    for (size_t i = 0; i < patt.length(); i++) {
        if (!check_char(patt[i], s[i + pos])) return false;
    }
    return true;
}

bool check_middles(vector<string> middles, string& subs) {
    if (middles.size() <= 1) return true;
    //cout << "middles size:" << middles.size() << "\n";
    size_t i = 0;
    for (string m : middles) {
        bool m_match = false;
        char f = m[0];
        char c = subs[i];
        if (m.length() == 0) m_match = true;
        //cout << "rozmiar m: "<<  m.size() << "\n";

        for (i; i < subs.length(); i++) {
            m_match = check_patt(m, subs, i);
            if (m_match) break;
        }
        if (!m_match) return false;
    }
    return true;
}

bool match(const string& pattern, const string& s) {
    // Divide into substrings - first, middles, last
    string first;
    string last;
    string middle;
    vector<string> middles;

    size_t star_a_pos = pattern.find_first_of('*');
    size_t star_z_pos = pattern.find_last_of('*');
    if (star_a_pos != -1) {
        first = pattern.substr(0, star_a_pos);
        last = pattern.substr(star_z_pos + 1);
        middle = pattern.substr(star_a_pos, star_z_pos - star_a_pos + 1);
        
        string subs = s.substr(first.length(), s.length() - last.length() - first.length());

        size_t star1 = 0;
        size_t star2 = 0;
        while (star2 != std::string::npos)
        {
            star2 = middle.find_first_of("*", star1 + 1);
            middles.push_back(middle.substr(star1 + 1, star2 - star1 - 1));
            star1 = star2;
        }

            /*cout << first << "/" << middle << "/" << last << "\n[" << subs << "]\n";
            for (string m : middles) {
                cout << m << "\n";
            }*/

            //cout << check_middles(middles, subs) << "\n";
        
            return (check_patt(first, s, 0) && check_middles(middles, subs) && check_patt(last, s, first.length() + subs.length()));

    }
    else {
        if (pattern.length() != s.length()) return false;
        return check_patt(pattern, s, 0);
    }
    //return true;
}

int main()
{
    string pattern = "ab**c";
    string s = "abc";

    cout << "Podaj pattern oraz s rozdzielone spacja:\n";
    cin >> pattern >> s;

    bool m = match(pattern, s);
    cout << "Wynik: " << m << "\n";
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln

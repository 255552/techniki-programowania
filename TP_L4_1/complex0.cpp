/* complex0.cpp */
/* Plik źródłowy klasy complex0.hpp */
#include <iostream>
#include "complex0.hpp"

// konstruktor domyślny 1+0i / (1,0)
Complex0::Complex0()
    : _real(1),
      _imag(0)
{
}

// konstruktor pobierający dwie liczby
Complex0::Complex0(double a, double b)
    : _real(a),
      _imag(b)
{
}

// konstruktor kopiujący
Complex0::Complex0(const Complex0 &other) : _real(other._real),
                                            _imag(other._imag)
{
}

// przeciążony operator przypisania
Complex0 &Complex0::operator=(const Complex0 &other)
{
    this->_real = other._real;
    this->_imag = other._imag;
    return *this;
}

// część rzeczywista
double Complex0::get_real() const
{
    return _real;
}

// część urojona
double Complex0::get_imag() const
{
    return _imag;
}

// ustawienie liczby zespolonej
void Complex0::set_cplx0(double a, double b)
{
    this->_real = a;
    this->_imag = b;
}

// • dodawanie dwóch liczb zespolonych,
Complex0 Complex0::operator+(const Complex0 &other)
{
    return Complex0(this->get_real() + other.get_real(),
                    this->get_imag() + other.get_imag());
}

// • dodawanie liczby zespolonej i liczby rzeczywistej,
Complex0 Complex0::operator+(const double &a)
{
    return Complex0(this->get_real() + a,
                    this->get_imag());
}

// • dodawanie liczby rzeczywistej i liczby zespolonej,
Complex0 operator+(const double a, const Complex0 &other)
{
    return Complex0(a + other.get_real(),
                    other.get_imag());
}

// • odejmowanie dwóch liczb zespolonych,
Complex0 Complex0::operator-(const Complex0 &other)
{
    return Complex0(this->get_real() - other.get_real(),
                    this->get_imag() - other.get_imag());
}

// • odejmowanie liczby zespolonej i liczby rzeczywistej,
Complex0 Complex0::operator-(const double &a)
{
    return Complex0(this->get_real() - a,
                    this->get_imag());
}

// • odejlmowanie liczby rzeczywistej i liczby zespolonej,
Complex0 operator-(const double a, const Complex0 &other)
{
    return Complex0(a - other.get_real(),
                    other.get_imag());
}

// • mnożenie dwóch liczb zespolonych,
Complex0 Complex0::operator*(const Complex0 &other)
{
    double a = this->get_real();
    double b = this->get_imag();
    double c = other.get_real();
    double d = other.get_imag();
    return Complex0(a * c - c * d, a * d + b * c);
}

// • mnożenie liczby zespolonej przez liczbę rzeczywistą,
Complex0 Complex0::operator*(const double a)
{
    return Complex0(a * this->get_real(), a * this->get_imag());
}

// • mnożenie liczby rzeczywistej przez liczbę zespoloną,
Complex0 operator*(const double a, const Complex0 &other)
{
    return Complex0(a * other.get_real(), a * other.get_imag());
}

Complex0 Complex0::conj()
{
    return Complex0(this->get_real(), -this->get_imag());
}

std::ostream& operator<<(std::ostream& out, const Complex0& c)
{
    double a = c.get_real();
    double b = c.get_imag();
    if(b>=0){
        return out << "(" << a << " + " << b << "i)";
    }
      return out << "(" << a << " - " << std::abs(b) << "i)";
}

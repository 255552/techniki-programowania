#include <iostream>
#include <vector>
#include "complex0.hpp"

using namespace std;

int main()
{
    Complex0 c1(2, 1);
    Complex0 c2(2, 3);
    Complex0 c3 = c1.conj();
    cout << c1 + c2 << "\n"
         << 5 - c2 << "\n"
         << 3 + c1 << "\n"
         << 3 * c1 << "\n"
         << c3 << "\n";
    return 0;
}

#pragma once
/* complex0.hpp */
/* Plik nagłówkowy klasy complex0.hpp */
#ifndef COMPLEX0_HPP_INCLUDED
#define COMPLEX0_HPP_INCLUDED

#include <iostream>

class Complex0
{
public:
    // konstruktor domyślny 1+0i / (1,0)
    Complex0();
    // konstruktor pobierający dwie liczby
    Complex0(double, double);
    // konstruktor kopiujący
    Complex0(const Complex0 &other);
    // kompilator zarządza pamięcią, więc destruktor domyślny
    // ~complex0();

    // przeciążony operator przypisania
    Complex0 &operator=(const Complex0 &other);

    // get, set
    double get_real() const;
    double get_imag() const;
    void set_cplx0(double, double);

    // • dodawanie dwóch liczb zespolonych,
    Complex0 operator+(const Complex0 &other);

    // • dodawanie liczby zespolonej i liczby rzeczywistej,
    Complex0 operator+(const double &other);

    // • dodawanie liczby rzeczywistej i liczby zespolonej,
    friend Complex0 operator+(const double a, const Complex0 &other);

    // • odejmowanie dwóch liczb zespolonych,
    Complex0 operator-(const Complex0 &other);

    // • odejmowanie liczby zespolonej i liczby rzeczywistej,
    Complex0 operator-(const double &other);

    // • odejmowanie liczby rzeczywistej i liczby zespolonej,
    friend Complex0 operator-(const double a, const Complex0 &other);

    // • mnożenie dwóch liczb zespolonych,
    Complex0 operator*(const Complex0 &other);

    // • mnożenie liczby zespolonej przez liczbę rzeczywistą,
    Complex0 operator*(const double a);

    // • mnożenie liczby rzeczywistej przez liczbę zespoloną,
    friend Complex0 operator*(const double a, const Complex0 &other);
    
    // • sprzężenie liczby zespolonej,
    Complex0 conj();

private:
    double _real;
    double _imag;
};

// • wypisanie do strumienia ostream.
std::ostream& operator<<(std::ostream &out, const Complex0 &c);

#endif // COMPLEX0_HPP_INCLUDED